package pages.firstPageObject;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pages.BasePage;
import utils.Waiters;

public class GoogleResultPage extends BasePage {

    public static final String URL_AUTOPRACTICE = "http://automationpractice.com/index.php";

    @FindBy(xpath = "//a[@href=\"http://automationpractice.com/\"]")
    private WebElement clickBySearchLink;

    public GoogleResultPage(WebDriver driver) {
        super(driver);
    }

    public void openUrlByName() {
        clickBySearchLink.sendKeys(Keys.ENTER);
        //clickBySearchLink.click();
        Waiters.waitForUrl(driver, Waiters.TIME_TEN, URL_AUTOPRACTICE);
    }

}
