import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.*;
import pages.firstPageObject.GoogleResultPage;
import pages.firstPageObject.GoogleSearchPage;
import static pages.firstPageObject.GoogleResultPage.URL_AUTOPRACTICE;

public class MyFirstTest {

    protected static GoogleSearchPage googleSearchPage;
    protected static GoogleResultPage googleResultPage;
    protected static WebDriver driver;


    @BeforeSuite
    public static void startDriver() {
        System.setProperty("webdriver.chrome.driver", "C:\\Projects\\selenium_project\\chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");
        driver = new ChromeDriver(options);
    }

    @BeforeClass
    public static void beforeMethod() {
        googleSearchPage = new GoogleSearchPage(driver);
        googleResultPage = new GoogleResultPage(driver);
    }

    @Test
    public void myFirstTest() {
        //Given
        googleSearchPage.open();
        //When
        googleSearchPage.search("automationpractice");
        //And
        //
        googleResultPage.openUrlByName();
        //Then
        Assert.assertEquals(URL_AUTOPRACTICE, driver.getCurrentUrl());
    }

    @AfterSuite
    public static void quitDriver() {
        driver.quit();
    }
}


