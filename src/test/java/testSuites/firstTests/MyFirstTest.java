package testSuites.firstTests;

import org.testng.Assert;
import org.testng.annotations.*;
import pages.firstPageObject.*;
import testSuites.BaseTest;

import static pages.firstPageObject.GoogleResultPage.URL_AUTOPRACTICE;

public class MyFirstTest extends BaseTest {

    protected static GoogleSearchPage googleSearchPage;
    protected static GoogleResultPage googleResultPage;

    @BeforeMethod
    public static void beforeMethod() {
        googleSearchPage = new GoogleSearchPage(getDriver());
        googleResultPage = new GoogleResultPage(getDriver());
    }

    @Test
    public void myFirstTest() {
        //Given
        googleSearchPage.open();
        //When
        googleSearchPage.search("automationpractice");
        //And
        googleResultPage.openUrlByName();
        //Then
        Assert.assertEquals(URL_AUTOPRACTICE, getDriver().getCurrentUrl());
    }

}


